{pkgs, ...}: let
  inherit (pkgs.inputs) crane;
  craneLib = crane.mkLib pkgs;
  buildInputs = with pkgs; [openssl.dev];
  nativeBuildInputs = with pkgs; [rust-bin.stable.latest.default pkg-config];
  buildEnvVars = {
    inherit buildInputs nativeBuildInputs;
    src = craneLib.path ../sonic-screwdriver;
    PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkg-config";

    cargoExtraArgs = "-p sonic-http";

    pname = "sonic-screwdriver";
    version = "1.0.0";
  };

  deps = craneLib.buildDepsOnly ({
      doCheck = false;
    }
    // buildEnvVars);
  project = craneLib.buildPackage ({
      cargoArtifacts = deps;
      doCheck = false;
    }
    // buildEnvVars);
in
  pkgs.stdenv.mkDerivation {
    name = "sonic-screwdriver";

    dontUnpack = true;
    buildInputs = [pkgs.makeWrapper];
    installPhase = ''
      mkdir -p $out/{bin,share};
      cp ${project}/bin/* $out/bin/;
      cp -r ${../sonic-screwdriver/static} $out/share/static/;
      cp -r ${../sonic-screwdriver/templates} $out/share/templates/;
      wrapProgram $out/bin/sonic-http \
          --set-default SONIC_STATIC_DIR "$out/share/static/" \
          --set-default SONIC_TEMPLATE_DIR "$out/share/templates/";
    '';
  }
