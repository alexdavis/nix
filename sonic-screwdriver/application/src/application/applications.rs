use anyhow::{anyhow, bail, Context, Result};

use super::{
    errors::{log_hidden_err, HideErrorExt},
    App,
};
use crate::adapters::{applications::ApplicationRow, DBError};
use sonic_types::{application::PatchApplication, Password};

impl App {
    pub async fn submit_application(&self, app: PatchApplication) -> Result<()> {
        let email_verify_token = Password::gen_random();

        self.email
            .send_application_confirm_email(&app, &email_verify_token)
            .await
            .hide_err("Error sending verification email.")?;

        self.applications
            .insert(&app, &email_verify_token)
            .await
            .hide_err("Error creating application")?;

        self.discord
            .notify_application_submit()
            .await
            .hide_err("Error sending discord notification.")?;

        Ok(())
    }

    pub async fn verify_application(&self, token: String) -> Result<()> {
        match self.applications.verify(&token).await {
            Ok(true) => Ok(()),
            Ok(false) => Err(anyhow!("Token is invalid")),
            Err(e) => {
                log_hidden_err("Error verifying application", e);
                Err(anyhow!("Internal error, please try again later."))
            }
        }
    }

    pub async fn get_application(&self, id: i32) -> Result<ApplicationRow> {
        match self.applications.by_id(id).await {
            Ok(x) => Ok(x),
            Err(DBError::NotFound) => Err(anyhow!("Could not find application with that id")),
            e => e.hide_err("Error getting application by id"),
        }
    }

    pub async fn pending_applications(&self) -> Result<Vec<ApplicationRow>> {
        self.applications
            .pending()
            .await
            .hide_err("Error getting pending applications")
    }

    pub async fn update_application(&self, id: i32, app: PatchApplication) -> Result<()> {
        self.applications
            .update(id, app)
            .await
            .hide_err("Error updating application")
    }

    pub async fn accept_application(&self, sponsor: Option<String>, id: i32) -> Result<()> {
        let application = self
            .applications
            .by_id(id)
            .await
            .hide_err("Error getting application by id")?;

        if application.accepted.is_some() {
            bail!("Decision already made");
        }

        // Add user
        let user = application
            .try_into_user(sponsor)
            .context("Error validating new user")?;

        let add_result = self.add_user(&user).await.context("Error creating user")?;

        self.email
            .send_welcome_email(&user, &add_result)
            .await
            .context("Error sending welcome email")?;

        self.applications
            .accept(id)
            .await
            .context("Error marking application as accepted")?;

        Ok(())
    }

    pub async fn deny_application(&self, id: i32) -> Result<()> {
        self.applications.deny(id).await
    }
}
