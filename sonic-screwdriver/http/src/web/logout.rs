use axum::{extract::State, response::Redirect, routing::get};
use axum_extra::extract::{cookie::Cookie, CookieJar};
use axum_flash::Flash;
use reqwest::StatusCode;
use std::sync::Arc;

use super::auth::{AuthState, UserInfo};
use crate::state::Router;

pub fn routes() -> Router {
    Router::new().route("/", get(logout))
}

/// Uses an [RP-Initiated Logout](https://openid.net/specs/openid-connect-rpinitiated-1_0.html)
/// provided in the
/// [`OpenID Connect` specification](https://openid.net/specs/openid-connect-core-1_0.html)
///
/// `OpenID Connect` is implemented ontop of `OAuth 2.0`
async fn logout(
    State(auth): State<Arc<AuthState>>,
    cookies: CookieJar,
    _user: UserInfo,
    flash: Flash,
) -> Result<(CookieJar, Flash, Redirect), StatusCode> {
    let id_token = cookies.get("id_token").ok_or(StatusCode::BAD_REQUEST)?;
    let end_session_url = auth
        .generate_end_session_uri(id_token.value())
        .map_err(|e| {
            dbg!(e);
            StatusCode::BAD_REQUEST
        })?;

    // Clean up any tokens
    let cookies = cookies
        .remove(Cookie::named("id_token"))
        .remove(Cookie::named("jwt"));

    Ok((
        cookies,
        flash.success("Logged out successfully."),
        Redirect::to(&end_session_url),
    ))
}
