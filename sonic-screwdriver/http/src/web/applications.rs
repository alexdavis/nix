use std::sync::Arc;

use axum::{
    extract::{Path, State},
    response::{IntoResponse, Redirect},
    routing::{get, post},
    Form,
};
use axum_flash::{Flash, IncomingFlashes};
use serde::Deserialize;
use sonic_application::App;
use sonic_types::web::apply::ApplyRequest;

use crate::{
    context, flash::IncomingFlashesExt, state::Router, template::render, utils::ErrorResponses,
};

use super::auth::UserIsAdmin;

pub fn routes() -> Router {
    Router::new()
        .route("/applications", get(list_applications))
        .route("/applications/:id", get(show_application))
        .route("/applications/:id", post(update_application))
        .route("/applications/:id/deny", post(deny_application))
        .route("/applications/:id/approve", post(approve_application))
}

async fn list_applications(
    _admin: UserIsAdmin,
    flashes: Flash,
    incoming_flashes: IncomingFlashes,
    State(app): State<Arc<App>>,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let applications = app
        .pending_applications()
        .await
        .err_to_redirect(&flashes, "/")?;

    let templ = render(
        "all_applications.html",
        context! {
            applications: applications,
            msgs: incoming_flashes.for_template(),
        },
    );
    Ok((incoming_flashes, templ))
}

async fn show_application(
    _admin: UserIsAdmin,
    Path(id): Path<i32>,
    app: State<Arc<App>>,
    flashes: Flash,
    incoming_flashes: IncomingFlashes,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let application = app
        .get_application(id)
        .await
        .err_to_redirect(&flashes, "/")?;

    let username_taken = if let Ok(username) = application.preferred_username.clone().try_into() {
        match app.is_username_taken(&username).await {
            Ok(x) => x,
            Err(e) => {
                println!("Error checking if username is taken: {:#}", e);
                true
            }
        }
    } else {
        false
    };

    let templ = render(
        "application.html",
        context! {
            application: application,
            msgs: incoming_flashes.for_template(),
            username_taken: username_taken
        },
    );
    Ok((incoming_flashes, templ))
}

async fn update_application(
    _admin: UserIsAdmin,
    State(app): State<Arc<App>>,
    Path(id): Path<i32>,
    flash: Flash,
    Form(update): Form<ApplyRequest>,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let detail_url = format!("/applications/{}", id);

    app.update_application(
        id,
        update
            .to_owned()
            .try_into()
            .err_to_redirect(&flash, &detail_url)?,
    )
    .await
    .err_to_redirect(&flash, &detail_url)?;

    Ok((
        flash.success("Updated application"),
        Redirect::to(&detail_url),
    ))
}

#[derive(Debug, Deserialize)]
struct DecisionBody {
    sponsor: Option<String>,
}

async fn approve_application(
    _admin: UserIsAdmin,
    Path(id): Path<i32>,
    State(app): State<Arc<App>>,
    flash: Flash,
    Form(decision): Form<DecisionBody>,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    // Mark application as denied
    app.accept_application(decision.sponsor, id)
        .await
        .err_to_redirect(&flash, &format!("/applications/{}", id))?;

    Ok((
        flash.success("Approved application!"),
        Redirect::to("/applications"),
    ))
}

async fn deny_application(
    _admin: UserIsAdmin,
    Path(id): Path<i32>,
    State(app): State<Arc<App>>,
    flash: Flash,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    // Mark application as denied
    app.deny_application(id)
        .await
        .err_to_redirect(&flash, &format!("/applications/{}", id))?;

    Ok((
        flash.success("Denied application. Consider emailing them to explain why."),
        Redirect::to("/applications"),
    ))
}
