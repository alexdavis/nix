Hi {{ full_name }},

You're receiving this email because you recently applied for a Tardis account.
In order to verify your email, please click the link below.

https://console.tardisproject.uk/verify/{{token}}

If you didn't apply for a Tardis account, you can safely disregard this email, and you won't receive any more.

Sincerely,
Tardis Admins.
