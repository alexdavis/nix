CREATE TABLE IF NOT EXISTS "pw_reset" (
       "token" text(50) NOT NULL PRIMARY KEY,
       "email" text(150) NOT NULL,
       "expires_at" text NOT NULL
);
