CREATE TABLE IF NOT EXISTS "domain" (
       "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
       "owner" text(50) NOT NULL,
       "domain" text(100)
);

CREATE TABLE IF NOT EXISTS "endpoint" (
       "prefix" text(50) NOT NULL,
       "domain_id" INTEGER NOT NULL,
       "target_ip" text(15) NOT NULL,
       "target_port" INTEGER NOT NULL,
       "target_is_https" BOOLEAN NOT NULL,
       PRIMARY KEY (prefix, domain_id),
       FOREIGN KEY(domain_id) REFERENCES domain(id)
);
