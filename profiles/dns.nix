{
  config,
  lib,
  ...
}: {
  networking.firewall.allowedUDPPorts = [53 5300];
  networking.firewall.allowedTCPPorts = [53 5300 5380];

  services.powerdns = let
    secondaries = "78.47.120.45,209.16.157.42,95.179.232.160,69.65.50.192,2001:1850:1:5:800::6b";
  in {
    enable = true;
    extraConfig = ''
      launch=gmysql
      gmysql-socket=/run/mysqld/mysqld.sock
      gmysql-user=pdns
      gmysql-dbname=pdns
      gmysql-dnssec=yes

      local-port=5300

      primary=yes
      allow-axfr-ips=${secondaries}

      webserver=yes
      webserver-allow-from=127.0.0.1,192.168.0.0/24
      webserver-address=0.0.0.0
      webserver-port=5380
      webserver-password=$scrypt$ln=10,p=1,r=8$zxrDzwJdj2N+rWPb4SfaTQ==$mUg8Nzxrj2F7rlxvkFGSWwvotcCrUASqdm16B8zpQpY=
      api=yes
      api-key=$scrypt$ln=10,p=1,r=8$zxrDzwJdj2N+rWPb4SfaTQ==$mUg8Nzxrj2F7rlxvkFGSWwvotcCrUASqdm16B8zpQpY=

      dnsupdate=yes
    '';
  };
  users.users.pdns.extraGroups = ["mysql"];
  services.mysql = {
    enable = true;
    ensureUsers = [
      {
        name = "pdns";
        ensurePermissions = {
          "pdns.*" = "ALL PRIVILEGES";
        };
      }
      {
        name = "powerdnsadmin";
        ensurePermissions = {
          "pdns.*" = "ALL PRIVILEGES";
        };
      }
    ];
    ensureDatabases = ["pdns"];
  };
  services.powerdns-admin = {
    enable = true;
    saltFile = "/etc/powerdns-admin/salt";
    secretKeyFile = "/etc/powerdns-admin/secret";
    config = ''
      BIND_ADDRESS = '127.0.0.1'
      PORT = 8000
      SQLALCHEMY_DATABASE_URI = 'mysql://powerdnsadmin@localhost/powerdnsadmin'
      MAIL_SERVER = 'mail.tardisproject.uk'
      MAIL_PORT = 587
      MAIL_USE_TLS = True
      MAIL_USERNAME = 'powerdns@tardisproject.uk'
      with open("/etc/powerdns-admin/mailPassword", "r") as f:
           MAIL_PASSWORD = f.read()
      MAIL_DEFAULT_SENDER = ('PowerDNS Admin', 'powerdns@tardisproject.uk')
      FILESYSTEM_SESSIONS_ENABLED = True
    '';
  };
  systemd.services."powerdns-admin".after = ["mysql.service"];
  systemd.services."powerdns-admin".serviceConfig = {
    BindPaths = ["/run/mysqld/mysqld.sock"];
    BindReadOnlyPaths = ["/etc/powerdns-admin/mailPassword"];
    TemporaryFileSystem = lib.mkOverride 10 ["/:ro" "/flask_session:mode=0777"];
  };
  services.unbound = {
    enable = true;
    settings = {
      server = {
        interface = ["0.0.0.0" "::0"];
        access-control = "192.168.0.0/24 allow";
        private-domain = "tardisproject.uk.";
      };
      remote-control = {
        control-enable = true;
      };
    };
  };
}
