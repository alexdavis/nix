{pkgs, ...}: {
  networking.firewall.allowedTCPPorts = [80 8443];

  # security.acme.certs."id.internal.tardisproject.uk" = lib.consts.ourACME // {
  #   keyType = "rsa2048";
  #   webroot = null;
  #   listenHTTP = ":80";
  #   reloadServices = ["keycloak.service"];
  # };
  services.keycloak = {
    enable = true;
    sslCertificate = "/var/lib/acme/id.internal.tardisproject.uk/cert.pem";
    sslCertificateKey = "/var/lib/acme/id.internal.tardisproject.uk/key.pem";
    database.passwordFile = "/run/keys/db_password";
    settings = {
      hostname = "id.tardisproject.uk";
      http-port = 8081;
      https-port = 8443;
      proxy = "reencrypt";
    };
    themes = {
      tardis = pkgs.stdenv.mkDerivation {
        name = "tardis-keycloak-theme";
        src = ./theme;
        phases = ["installPhase"];
        installPhase = ''
          mkdir -p $out
          cp -r $src/* $out
        '';
      };
    };
  };
  users = {
    users.keycloak = {
      isSystemUser = true;
      group = "keycloak";
      extraGroups = ["krb5"];
    };
    groups.keycloak = {};
  };
}
