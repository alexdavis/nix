{
  pkgs,
  config,
  lib,
  inputs,
  ...
}: {
  networking.firewall.allowedTCPPorts = [19999];

  age.secrets.discordWebhook = {
    file = ../../secrets/discordWebhook.age;
    owner = "netdata";
    group = "netdata";
  };

  services.netdata = {
    enable = true;
    config = {
      db = {
        "update every" = 30;
      };
      ml = {enabled = "no";};
      health = {
        "run at least every seconds" = 600;
        "enabled alarms" = "!httpcheck_web_service_timeouts *";
      };
    };
    configDir = {
      "stream.conf" = let
        mkChildNode = apiKey: allowFrom: ''
          [${apiKey}]
            enabled = yes
            default memory mode = dbengine
            health enabled by default = auto
            allow from = ${allowFrom}
        '';
      in
        pkgs.writeText "stream.conf" ''
          [stream]
            enabled = no
            enable compression = yes

          [35c10a2e-ee17-4d27-81c1-ee15d3103598]
            enabled = yes
            default memory mode = dbengine
            health enabled by default = auto

          ${mkChildNode "8e5db52a-8c9b-403d-813c-08ba3e0e1899" "192.168.0.12"}
          ${mkChildNode "5332994a-ed48-4a4d-90f7-00f727793f3b" "192.168.0.14"}
        '';
      "health_alarm_notify.conf" = pkgs.writeText "netdata-discord-notify" ''
        SEND_DISCORD="YES"
        DISCORD_WEBHOOK_URL=`cat ${config.age.secrets.discordWebhook.path}`
        DEFAULT_RECIPIENT_DISCORD="tardis-bots"
      '';
    };
  };

  # wait-for-netdata-up is broken on new versions
  systemd.services.netdata.serviceConfig.ExecStartPost = lib.mkForce [];
}
