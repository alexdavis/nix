{lib, ...}: {
  security = {
    krb5 = {
      enable = true;
      settings = {
        realms = {
          "TARDISPROJECT.UK" = {
            kdc = [(lib.subdomain "kdc.internal")];
            admin_server = [(lib.subdomain "kadm.internal")];
            default_domain = "tardisproject.uk";
          };
        };
        domain_realm = {
          "tardisproject.uk" = "TARDISPROJECT.UK";
          ".tardisproject.uk" = "TARDISPROJECT.UK";
        };
        libdefaults = {
          default_realm = "TARDISPROJECT.UK";
          dns_lookup_kdc = false;
          dns_lookup_realm = false;
        };
        appdefaults = {
          default_realm = "TARDISPROJECT.UK";
          dns_lookup_kdc = false;
          dns_lookup_realm = false;
        };
      };
    };
  };

  # Used for files like keytabs
  users.groups = {"krb5" = {};};
}
