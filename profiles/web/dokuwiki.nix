{
  config,
  pkgs,
  ...
}: {
  services.caddy.virtualHosts."http://wiki.tardisproject.uk".hostName = "wiki.tardisproject.uk";

  age.secrets = {
    dokuwikiOauthSecret = {
      file = ../../secrets/dokuwikiOauthSecret.age;
      owner = config.services.phpfpm.pools."dokuwiki-wiki.tardisproject.uk".user;
      group = config.services.phpfpm.pools."dokuwiki-wiki.tardisproject.uk".group;
      mode = "0440";
    };
    dokuwikiEmailUser = {
      file = ../../secrets/dokuwikiEmailUser.age;
      owner = config.services.phpfpm.pools."dokuwiki-wiki.tardisproject.uk".user;
      group = config.services.phpfpm.pools."dokuwiki-wiki.tardisproject.uk".group;
      mode = "0440";
    };
    dokuwikiUsersFile = {
      file = ../../secrets/dokuwikiUsersFile.age;
      owner = config.services.phpfpm.pools."dokuwiki-wiki.tardisproject.uk".user;
      group = config.services.phpfpm.pools."dokuwiki-wiki.tardisproject.uk".group;
      mode = "0600";
    };
  };

  services.dokuwiki = {
    webserver = "caddy";
    sites."wiki.tardisproject.uk" = {
      usersFile = config.age.secrets.dokuwikiUsersFile.path;
      settings = {
        # General
        title = "TARDIS Project";
        superuser = "@timelords";
        useacl = true;
        userewrite = true;
        disableactions = "register,resendpwd,profile_delete";
        hidepages = "^:(.*sidebar|playground|labels|templates|.*_*template)";
        useheading = "navigation";
        dontlog = "error,debug,deprecated"; # currently spams a warning which fills up the disk

        # Auth
        authtype = "oauth";
        plugin.oauth = {
          # This setting should, in theory, allow the only sign-in option to be SSO. However, this,
          # for some reason, doesn't work. Instead, you can only click on "log out". So until
          # someone else wants to experiment with it, keep this commented out.
          # singleService = true;
          register-on-auth = true;
        };
        plugin.oauthkeycloak = {
          openidurl = "https://id.tardisproject.uk/realms/master/.well-known/openid-configuration";
          label = "TARDIS Account";
          color = "#333333";
          key = "wiki.tardisproject.uk";
          secret._file = config.age.secrets.dokuwikiOauthSecret.path;
        };

        # API Access
        remote = true;
        remoteuser = "@api";

        mailfrom = "dokuwiki@tardisproject.uk";
        plugin.smtp = {
          smtp_host = "localhost";
          smtp_port = 25;
          smtp_ssl = "tls";
          localdomain = "tardisproject.uk";
          auth_user = "dokuwiki@tardisproject.uk";
          auth_pass._file = config.age.secrets.dokuwikiEmailUser.path;
        };
      };
      pluginsConfig = {
        oauth = true;
        oauthkeycloak = true;
        smtp = true;
      };
      acl = [
        {
          page = "*";
          actor = "@ALL";
          level = "read";
        }
        {
          page = "*";
          actor = "@user";
          level = "upload";
        }
        {
          page = "*";
          actor = "@api";
          level = "create";
        }
        {
          page = "secret:*";
          actor = "@ALL";
          level = "none";
        }
        {
          page = "secret:*";
          actor = "@timelords";
          level = "delete";
        }
      ];

      plugins = let
        pluginForSrcDef = srcDef:
          pkgs.stdenv.mkDerivation {
            name = srcDef.name;
            src = pkgs.fetchurl {
              url = srcDef.url;
              sha256 = srcDef.sha256;
            };
            sourceRoot = ".";
            buildInputs = [pkgs.unzip];
            installPhase = "mkdir -p $out; cp -R */* $out/";
          };
        pluginsForSrcDefs = map pluginForSrcDef;
      in
        pluginsForSrcDefs [
          {
            name = "oauth";
            url = "https://github.com/cosmocode/dokuwiki-plugin-oauth/archive/refs/tags/2023-03-27.zip";
            sha256 = "16d1c1mls12v6rni9c49qck81561bg3zznd0l0rbrd1k44kvfw66";
          }
          {
            name = "oauthkeycloak";
            url = "https://github.com/YoitoFes/dokuwiki-plugin-oauthkeycloak/archive/28892edb0207d128ddb94fa8a0bd216861a5626b.zip";
            sha256 = "038lwhjxfs96109cr1vp6pr0clp9xysz1y4g5lsxkzk2ypbxc6fp";
          }
          {
            name = "smtp";
            url = "https://github.com/splitbrain/dokuwiki-plugin-smtp/archive/refs/tags/2023-04-03.zip";
            sha256 = "110l586b3d129a8c1d26rypm06pq0kpkwi4fgxvxsyjb1bbpz5vb";
          }
          {
            name = "acmenu";
            url = "https://github.com/tormec/AcMenu/archive/refs/tags/v1.5.zip";
            sha256 = "0hq2fz1pz72cd3wky7z82lll1ck0g7wijgd9fgq9ba12scp37p4b";
          }
        ];
    };
  };

  services.caddy.virtualHosts."http://wiki.tardisproject.uk".logFormat = "output file ${config.services.caddy.logDir}/access-wiki.log {
        roll_keep_for 1d
  }";
}
